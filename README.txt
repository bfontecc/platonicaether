platonicaether README file:

Much of the code from this project will be based on other libraries, including
my own bitbucket:

https://bitbucket.org/bfontecc/project1

jQuery:

http://jquery.com/

jQuery Form plugin:

http://jquery.malsup.com/form/

mathJAX:

http://www.mathjax.org/

processing.js:

http://processingjs.org/

bootstrap:

http://twitter.github.io/bootstrap/


The user authentication features require a mysql database set up according to the constants declared in models/db.php and controllers/register.php

docs/db.txt shows mysql code which can be used to create such a database.

At this time, the scoring feature is not implemented anyway, so there is no practical need to be authenticated.



