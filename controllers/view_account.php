<?php
/*
 * controllers/view_account.php
 *
 * Checks whether the person is authenticated or not.
 *	Offers views/create_account if not. This includes a log in link.
 *	Offers views/my_account if not.
 */

if (!isset($auth)):
	$content = 'create_account';
elseif ($auth == false):
	$content = 'create_account';
else:
	$content = 'my_account';
endif; 
