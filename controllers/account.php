<?php
/*
 * controllers/account.php
 *
 * checks whether the user is logged in or not
 * offers content related to their account if they are logged in
 * offers login and sign up pages if not
 */

// if user IS logged in
if ($auth) {		// $auth is set by models/session.php
	$h2 = "My User Account";
	$content = 'my_account';
	include_once("controllers/my_account");
} else {
	$h2 = "Create an Account";
	$content = 'create_account';
	// this one doesn't need a special controller. It actually just has html, and posts to controller/register.php
}

?>
