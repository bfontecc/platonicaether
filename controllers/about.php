<?php
/*
 * controllers/about.php
 *
 * doesn't output any html
 * this is a basic controller which just sets the content to be loaded.
 */

// the account view content will be loaded by controllers/content_loader.php
$content = 'about';
?>
