<?php
/**
 * controllers/register.php
 *
 * This controller will accept form data via AJAX and return a message
 *
 * based on my old code from here:
 *
 * https://bitbucket.org/bfontecc/project1/src/2b818b8a613351146df69fc0aea7287d027f3f9c/controllers/register.php?at=master
 *
 * This version of register.php is not specific about why it fails because it largely relies on client-side
 *	validation. If the user is bypassing those scripts, then we will not be verbose with them because
 *	they could be a hacker or they have javascript turned off. If javascript is off they can't receive
 *	error messages from here anyway because they are sent via AJAX.
 *
 * success upon: registering the user using the functionality of models/auth.php and receiving 'true'
 * 	for the value of 'register_status' in the returned array
 * failure upon: 
 *	malformed email
 *	malformed password
 *	malformed email and password
 *	a value of 'false' returned in 'register_status'
 */


// this should be called from the context of this script's location
// i.e. this file is not expected to be included elsewhere
include("../models/db.php");
include("../models/session.php");

/*
 * These values are somewhat arbitrary, but they must be compatible the client side verification scripts,
 * the database settings, and any other verifications.
 */

define("EMAIL_MIN", 6);
define("EMAIL_MAX", 30);
define("PASS_MIN", 6);
define("PASS_MAX", 20);

define("ERROR_START", "Sorry! Your Account could not be created. ");
 
$email = isset($_POST['email']) ? $_POST['email'] : false;
$password = isset($_POST['pass']) ? $_POST['pass'] : false;

if (!$email || !$password):
	// failure - did not receive all the necessary form data
	echo ERROR_START . "Please enter an email and password. Remember to follow the guidelines.";
	return;
elseif (!email_well_formed($email)):
	// failure - invalid email
	echo ERROR_START . "Please enter a valid email address.";
	return;
elseif (!password_well_formed($password)):
	// failure - invalid password
	echo ERROR_START . "Please enter a valid password. See the guidelines below.";
else:
	// try registering using register_user function in models/auth.php
	$model_result = register_user($email, $password);
	if ($model_result['register_status']):
		// success 
		echo "Your account has been created. You may proceed to the login page.";
	else:
		//failure - error from database
		echo ERROR_START . "There was a problem with your request. Please try again or contact the administrator.";
	endif;
endif;


/**
 * email_well_formed()
 * calls functions check_length(), check_email_syntax(), escape_check()
 * @param string $email
 * @return [true | false]
 *
 * written for this project, loosely based on old code of mine in the above-mentioned bitbucket
 */
function email_well_formed($email) {
        $status = true;
        if (!check_length($email, EMAIL_MIN, EMAIL_MAX)):
		$status = false;
        elseif (!check_email_syntax($email)):
		$status = false;
        elseif (!escape_check($email)):
		$status = false;
        endif;
        return $status;
}
 
/**
 * password_well_formed()
 *
 * calls functions check_length(), is_homogenous(), escape_check()
 * @param string $password
 * @return [true | false]
 *
 * another rewrite. See comments for email_well_formed().
 */
function password_well_formed($password) {
        $status = true;
        if (!check_length($password, PASS_MIN, PASS_MAX)):
		$status = false;
        elseif (is_homogenous($password)):
		$status = false;
        elseif (!escape_check($password)):
		$status = false;
        endif;
        return $status;
}

/**
 * check_length()
 * length must be within or equal to min, max
 * @param numeric $min
 * @param numeric $max
 * @param string $str
 * @return bool $correct_len
 *
 * from my bitbucket as-is
 */
function check_length($min, $max, $str) {
        if (strlen($str) < $min || strlen($str) > $max):
                return false;
        else:
                return true;
        endif;
}

/**
 * check_email_syntax()
 * @param string $email
 * @return bool $correct_syntax
 *
 * bitbucket code
 */
function check_email_syntax($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * escape_check()
 * applies htmlspecialchars to a string, and determines whether it has changed
 * @param string $str
 * @return
 *
 * bitbucket code
 */
function escape_check($str) {
        if ($str != htmlspecialchars($str)):
                return false;
        else:
                return true;
        endif;
}

/**
 * is_homogenous()
 * @param string $password
 * @return bool $homogenous
 *
 * bitbucket code
 */
function is_homogenous($password) {
        $num_pattern = "/[0-9]/";
        $alpha_pattern = "/[a-zA-Z]/";
        if (preg_match($num_pattern, $password)
                && preg_match($alpha_pattern, $password)):
                return false;
        else:
                return true;
        endif;
}
