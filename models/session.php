<?php
/* 
 * models/session.php
 *
 * includes functions for accessing and modifying user session data
 *
 * requires models/auth.php
 * this is not required in code because this file may be included from different contexts.
 * it is better for the script including this file to also include models/db.php
 */

$auth = check_auth();

/**
 * set_session_is_auth()
 *
 * refactored out of database model. see models/auth.php
 *
 * sets the user's session 'auth' bool field to true
 * stores the user_id in the session.
 *
 * @param numeric $user_id
 */
function set_session_is_auth($user_id) {
        $_SESSION['auth'] = true;
        $_SESSION['user_id'] = $user_id;
}

/**
 * get_user_id()
 *
 * returns the user_id field from the session
 * returns false upon failure
 *
 * failure conditions include user not logged in, error in session code
 */
function get_user_id() {
	$uid = false;
	if (!empty($_SESSION)) {
		if (isset($_SESSION['user_id'])) {
			$uid = $_SESSION['user_id'];
		}
	}
	return $uid;
}

/**
 *
 * check_auth()
 *
 * checks if the user is authenticated
 * sets $auth variable
 */
function check_auth() {
	$authorized = false;
	if (!empty($_SESSION)) {
		if (isset($_SESSION['auth'])) {
			if ($_SESSION['auth'] === true) {
				$authorized = true;
			}
		}
	}
	return $authorized;
}
