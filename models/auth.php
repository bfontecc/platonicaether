<?php
/**
 * This code was written for csci e-75 with Professor Malan and TA Chris Gerber
 * It is used here as-is because I really can't think of any other way to implement this, besides
 *	using similar, publicly available code such as 'idiorm'.
 *
 * db_model.php
 * @author Bret Fontecchio
 *
 * The mysql database containing user login credentials will be maintained and accessed here.
 *
 * salting and hashing is done during query preparation
 *
 * written in 2012, accessed from bitbucket
 * https://bitbucket.org/bfontecc/project1/src/2b818b8a6133/models/db_model.php?at=master
 * 
 */

//database variables - based on Chris Gerber's source
define('DB_HOST', 'localhost');
define('DB_USER', 'plato');
define('DB_PASSWORD', 'FCzAJes668ARcZ4X');
define('DB_DATABASE', 'platonicaether');
define('SALT', '$$$');

/**
 * get_handle()
 * connect to mysql server, select database, and return handle
 * 
 * @return PDO Object $dbh
 */
 function get_handle() {
        $dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE, DB_USER, DB_PASSWORD);
        return $dbh;
 }
 
/**
 * salt_and_hash()
 * adds salt constant defined above to a string
 * uses SHA1 to hash the salted string
 *
 * @param string $pass
 * @return string $password_hash
 */
function salt_and_hash($pass) {
        $pass .= SALT;
        return hash("SHA1", $pass);
}

/**
 * get_check_query_combo()
 * Prepares a PDO SQL statement for checking whether an email and password combo
 * is in the users table and selects user_id for that row. 
 * Takes password, NOT password hash.
 *
 * @param PDO Object $dbh
 * @param string $email
 * @param string $pass
 *
 * @return PDOStatement Object $check_query
 */
function get_check_query_combo($dbh, $email, $pass) {
        $email = strtolower($email);
        $password_hash = salt_and_hash($pass);
        //prepare and bind query statement
        $check_query = $dbh->prepare("
					SELECT user_id FROM users 
					WHERE LOWER(email) = :email_bind
					AND password_hash = :hash_bind
					");
        $check_query->bindValue(':email_bind', $email, PDO::PARAM_STR);
        $check_query->bindValue(':hash_bind', $password_hash, PDO::PARAM_STR);
        return $check_query;
}

/**
 * get_check_query_email()
 * Prepares a PDO SQL statement for checking whether an email
 * is in the users table and selects user_id for that row. 
 *
 * @param PDO Object $dbh
 * @param string $email
 *
 * @return PDOStatement Object $check_query
 */
function get_check_query_email($dbh, $email) {
        $email = strtolower($email);
        //prepare and bind query statement
        $check_query = $dbh->prepare("
					SELECT user_id FROM users 
					WHERE LOWER(email) = :email_bind
					");
        $check_query->bindValue(':email_bind', $email, PDO::PARAM_STR);
        return $check_query;
}

/**
 * get_insert_user_query()
 * Prepares a PDO SQL statement for inserting a user email and pass into users
 * Takes password, NOT password hash.
 *
 * @param PDO Object $dbh
 * @param string $email
 * @param string $pass
 *
 * @return PDOStatement Object $check_query
 */
function get_insert_user_query($dbh, $email, $pass) {
        $email = strtolower($email);
        $password_hash = salt_and_hash($pass);
        //prepare and bind query statement
        $insert_query = $dbh->prepare("
					INSERT INTO users (email, password_hash, cash)
					VALUES (:email_bind, :hash_bind, 10000);
					");
        $insert_query->bindValue(':email_bind', $email, PDO::PARAM_STR);
        $insert_query->bindValue(':hash_bind', $password_hash, PDO::PARAM_STR);
        return $insert_query;
}

/**
 * check_user_id()
 * Determines whether there is one and only one user_id in an array returned by
 * PDOStatement::fetchAll
 * If so, returns it. If not, returns false.
 *
 * @param array $query_results
 * @return [false | numeric user_id]
 */
function check_user_id($query_results) {
        if (
                !isset($query_results[0]) 
                || !isset($query_results[0]['user_id'])
                || isset($query_results[1]['user_id'])
                ) {
                return false;
        } else {
                return $query_results[0]['user_id'];
        }
}

/**
 * register_user()
 * Create new row in table users.
 *
 * returns false on failure
 *
 * @param string $email
 * @param string $pass
 * 
 * @return array $status
 * array keys:
 * 'register_error' => string
 * 'register_status' => bool
 */
 function register_user($email, $pass) {
        $dbh = get_handle();
        //prepare PDO statements
        $check_query = get_check_query_email($dbh, $email);
        $insert_query = get_insert_user_query($dbh, $email, $pass);
        $dbh->beginTransaction();
        $check_query->execute();
        $check_result = $check_query->fetchAll();
        if (check_user_id($check_result)):
                $dbh->rollback();
                return array('register_status' => false, 'register_error' => 'That email address is taken.');
        else:
                if($insert_query->execute()):
                        $dbh->commit();
                        return array('register_status' => true, 'register_error' => 'Success. No error.');
                else:
                        $dbh->rollback();
                        return array('register_status' => false, 'register_error' => $insert_query->errorCode());
                endif;
        endif;
 }
  

 /**
  * user_login()
  * Check user credentials against database.
  * Return numeric user_id from users table
  *
  * Returns false on any failure to log in.
  *
  * @param string $email
  * @param string $pass
  *
  * @return int $user_id
  */
function user_login($email, $pass) {
        //get database handle PDO Object
        $dbh = get_handle();
        //prepare and bind query statement
        $check_query = get_check_query_combo($dbh, $email, $pass);
        //execute statement and fetch results
        $check_query->execute();
        $query_results = $check_query->fetchAll();
        //check and return user_id
        $user_id = check_user_id($query_results);
        return $user_id;
}

?>
