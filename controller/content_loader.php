<?php
/**
 * content_loader.php
 *
 * Loads the view for a particular type of content.
 * 
 * Places content into a bootstrap "well" and loads in parameters.
 * Should be required by default controller/dispatcher (/html/index.php).
 *
 * Taken from my personal bitbucket
 */
 echo 'hello from content_loader';
//include_once("../views/templates/header.php");
echo '<div class="well">';	//put the content in the well

if (!isset($content)) {
        $content = 'home';
}

include_once("../view/{$content}.php"); // this is the most important line

echo '</div>';