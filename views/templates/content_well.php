<?php
/**
 * views/templates/content_loader.php
 *
 * sets up a bootstrap well and loads the view specified by $content
 */ 
?>
		<div class="well span8">
			<h2 id='<? echo $content ?>'><? echo $h2 ?></h2>
		<?php 
			if (!isset($content)) {
				$content = 'about';
			}
			// $content should be set by index.php
			include_once("views/{$content}.php"); 
		?>
		</div>
