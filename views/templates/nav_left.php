<?php
/*
 * views/templates/nav_left.php
 *
 * creates left-hand navigation bar
 */
?>
		<div class="well span3">
			<ul class="nav nav-list">
				<li class="nav-header">Site Navigation</li>
				<li id="navabout"><a href="index.php?q=about">About</a></li>
				<li id="navaccount"><a href="index.php?q=account">Account</a></li>
				<ul class="nav nav-list">
					<li id="navlogin"><a href="index.php?q=login">Log In</a></li>
					<li id="navlogout"><a href="index.php?q=logout">Log Out</a></li>
					<li id="navcreate_account"><a href="index.php?q=create_account">Create Account</a></li>
					<li id="navview_account"><a href="index.php?q=view_account">View Account</a></li>
				</ul>
				<li class="nav-header">Trigonometry</li>
				<li id="navunit_circle"><a href="index.php?q=unit_circle">Unit Circle</a></li>
				<li id="navtrig_functions"><a href="index.php?q=trig_functions">Trig Functions</a></li>
			</ul>
		</div>
