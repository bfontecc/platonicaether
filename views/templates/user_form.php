<?php
/*
 * views/templates/user_form.php
 *
 * contains user and password fields
 *
 * does not include out form tags, due to the specific nature of the attributes
 */
?>

		<div class="control-group">
			<label class="control-label" for="email">Email:</label>
			<div class="controls">
				<input type="email" name="email" id="email" placeholder="user@example.com">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="pass">Password:</label>
			<div class="controls">
				<input type="password" name="pass" id="pass" placeholder="Password">
			</div>
		</div>
		<div class="control-group offset2">
			<button type="submit" class="btn">Register</button>
		</div>
