<?php
/*
 * header html content
 *
 * includes 
 *	HTML5 declaration
 *	bootstrap includes
 */

?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title> <?php echo $title ?> </title>
		<!--Twitter Bootstrap CSS Library-->
		<!--license: https://github.com/twitter/bootstrap/wiki/License-->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="css/main.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				var myh2id = $('h2').attr('id');
				var mynavid = '#nav' + myh2id;
				/* e.g. for about:
				 * myh2id is 'about'
				 * mynavid = '#navabout'
				 */
				$(mynavid).attr('class', 'active');
			});
		</script>
	</head>
	<body>
	<div class="container-fluid">
		<div class="well span12">
			<h1>Platonic Aether</h1>
		</div>
