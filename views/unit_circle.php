<?php
/*
 * views/unit_circle
 *
 * Demonstrate the unit circle to viewers using interactive functionality
 */
?>
<h3>Directions:</h3>
<p>Click the app and then use the UP and DOWN arrow keys to control the angle. Watch the trig functions change their ouputs at the bottom.</p>
<p>See the <a href="https://processing.org/learning/trig/">Tutorial</a> located here for an explanation of the unit circle and then come back to see it interactively. You can view the source code below and use the <a href="http://processing.org/reference/">Processing Language Reference</a> to modify this, or write your own Javascript trigonometry programs.</p>

<?php

include("processing/processing_content.php");

?>
