<?php
/**
 * views/create_account.php
 *
 * offers form to create user account
 * submits to register controller
 *
 * For this project, validation is done on the client side
 * This would normally be a security issue but I have decided not to include code I already own which verifies on the server side
 * 	because I want to incorporate lots of new skills learned in CS12 without relying on other resources too much.
 * In a production environment I would include the following in the project:
 * 	https://bitbucket.org/bfontecc/project1/src/2b818b8a6133/controllers/register.php?at=master
 */

?>

	<script src="http://malsup.github.com/jquery.form.js"></script> 
	<script> 
	// form AJAX, using jQuery form plugin
        $(document).ready(function() { 
		// set up options for jQuery form plugin
		var options = {
			beforeSubmit:	validate,
			error: 		onError,
			success:	onSuccess,	// this is only successful submission, not successful registration
		};
		// bind 'register' form and use options above
		$('#register').ajaxForm(options);
        }); 
	function onSuccess(responseText, statusText, xhr, $form) {
		$('#msg').text(responseText);
		if (statusText == 'success') {
			$('#register').hide();
		}
	}
	function onError() {
		$('#msg').text("Sorry. Your request could not be completed. Please try again or contact the Administrator.");	
	}
	function validate(formData, jqForm, options) {
		// my validation code
		// use bootstrap red and green
	}
	</script>
	
	<div id="msg"></div> 
	<div id="guidelines">
		<!-- put guidelines here -->
	</div>
	<form id="register" class="form-horizontal" method="post" action="controllers/register.php">
		<?php include("views/templates/user_form.php"); ?>
	</form>

	<div id="login">
		Already have an account? <a href="index.php?q=login">Click Here</a> to log in.
	</div>
