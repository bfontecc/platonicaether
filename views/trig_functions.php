<?php
/*
 * views/trig_functions
 *
 * uses mathJAX to display trig identities
 */

?>
		<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
		<h3>SOH CAH TOA</h3>
		<table class="table table-striped table-bordered">
			<caption>Trigonometric Identity Functions</caption>
			<thead>
				<tr>
					<th>Function</th>
					<th>Mnemonic</th>
					<th>Identity</th>
				</tr>
			</thead>
			<tbody>
				<tr id="sin">
					<td>Sine (sin)</td>
					<td>
						<math>
							<mfrac>
								<mtext>opposite</mtext>
								<mtext>hypotenuse</mtext>
							</mfrac>
						</math>
					</td>
					<td>
						<math>
							<mtext>sin</mtext>
							<mo>(</mo><mi>&theta;</mi><mo>)</mo>
							<mo>=</mo>
							<mtext>cos</mtext>
							<mo>(</mo>
								<mfrac><mi>&pi;</mi><mn>2</mn></mfrac>
								<mo>-</mo>
								<mi>&theta;</mi>
							<mo>)</mo>
						</math>
					</td>
				</tr>
				<tr id="cos">
					<td>Cosine (cos)</td>
					<td>
						<math>
							<mfrac>
								<mtext>adjacent</mtext>
								<mtext>hypotenuse</mtext>
							</mfrac>
						</math>
					</td>
					<td>
						<math>
							<mtext>cos</mtext>
							<mo>(</mo><mi>&theta;</mi><mo>)</mo>
							<mo>=</mo>
							<mtext>sin</mtext>
							<mo>(</mo>
								<mfrac><mi>&pi;</mi><mn>2</mn></mfrac>
								<mo>-</mo>
								<mi>&theta;</mi>
							<mo>)</mo>
						</math>
					</td>
					<tr id="tan">
					<td>Tangent (tan)</td>
					<td>
						<math>
							<mfrac>
								<mtext>opposite</mtext>
								<mtext>adjacent</mtext>
							</mfrac>
						</math>
					</td>
					<td>
						<math>
							<mtext>tan</mtext>
							<mo>=</mo>
							<mfrac>
								<mtext>sin</mtext>
								<mtext>cos</mtext>
							</mfrac>
						</math>
					</td>
				</tr>
				</tr>
			</tbody>
		</table>
