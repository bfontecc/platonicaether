<?php
/* 
 * views/logout.php
 *
 */
?>
	<p>You are now logged out! Use the navigation bar at left to log in again or create a new account.</p>
