<?php
/*
 * views/login.php
 *
 * Offers form for login data. 
 */

if ($auth):
	echo 'You are already logged in. <a href="index.php?q=logout">Click Here</a> to log out.';
else:
	include("views/templates/user_form.php");
endif;

?>
