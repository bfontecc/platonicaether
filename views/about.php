<?php
/*
 * views/about.php
 *
 *
 */
?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script> 
                $(function() { 
                        $('#accordion').accordion({ 
                                collapsible: true 
                        }); 
                }); 
</script>
<div id="accordion">
	<h3>What is a Platonic Aether?</h3>
	<div>
		<p>Plato's <i>Timaeus</i> offers a mythological account of the world's creation by a divine craftsman, or demiurge. According to Plato, the universe began as pure chaos but was skillfully restructured after the heavens and imparted with order. Plato believed that forms, such as a triangle, are models of heavenly, perfect forms which we can understand through "true knowledge", or pure understanding. This knowledge is different from the knowledge of instruction, which convinces us to believe something. True knowledge is not an opinion or fact, it is the moment when you recognize order in the world, chaotic as it may be, and you can see the forms that underlie its workings. For example, although a triangle we may draw on paper is not a perfect triangle, it suggests to us a trianglular form, and this form we know is perfectly proportioned and straight. Therefore, the diagram reminds us of something we already know, something heavenly that transcends material "stuff". </p>
	</div>
	<h3>What's it have to do with me?</h3>
	<div>
<p>These Days, not many physicists believe Plato's story of creation. We can, however, still recognize that moment when something "clicks" and becomes true knowledge. For Plato, it was an act of remembering something, reminiscing on what we knew before our soul left the orderly heavens.</p>
		<p>The basic heavenly form, the essence that carries all else, is the aether (pronounced EE-ther). The goal of this website is to impart upon visitors some learning experiences that seem more like remembering something you already knew than being lectured or taught.</p>
		<p>This will mostly be done with visual representations of some common forms like trigonometry. This site is very much a work in progress and is currently in an extremely early stage.</p> 
		<p>This site is largely inspired by the book <i>Philosophy of Mathematics</i>, by James Robert Brown. Brown argues for mathematicians to take visual diagrams seriously as an instrument of mathematics, as opposed to simply decoration for explanations. Brown gives examples of "picture proofs." An admirer of Plato, Brown argues that this proofs can impart true knowledge.</p>
	</div>
		<h3>What can I find on this site?</h3>
	<div>
		<p>This site is still a work in progress, but at the moment you can find some basic examples introductory Trigonometry material by using the Navigation bar at the left of the page.</p>
		<p>As time goes on, I hope to add the following features:
			<ul>
				<li>Scoring for example problems</li>
				<li>Continously updated analysis of score progress</li>
				<li>Additional lessons such as Probability and Discrete Math</li>
			</ul>
		</p>
	</div>
</div>
