<?
/**
 * index.php
 *
 * default dispatcher
 * loads controller for requested page
 *
 * Note that the controller name is also the h2 id and navigation id for that content.
 * The link "active" functionality depends on this.
 *
 * Some links will be automatically loaded other content. For example an unauthenticated user accessing
 *	my_account will be served create_account
 */

// this needs to be done first before headers are sent
session_start();

$title = "Platonic Aether";
$h2 = '';
$nav_id = '';

// is there is no content requested by ?q=
if (empty($_GET)) {
	$controller = 'about';
	$h2 = 'About Platonic Aether';
	$nav_id = 'about';
} elseif (!isset($_GET['q'])) {
	$controller = 'about';
	$h2 = 'About Platonic Aether';
	$nav_id = 'about';
} else {	// if there IS content requested i.e. q does have a value
	switch ($_GET['q']) {	// considered $content = $_GET['q'] but this is safer
		case 'login':
			$controller = 'login';
			$h2 = 'Login';
			break;
		case 'logout':
			$controller = 'logout';
			$h2 = 'Logout';
			break;
		case 'about':
			$controller = 'about';
			$h2 = 'About Platonic Aether';
			break;
		case 'account':
			$controller = 'account';
			$h2 = 'My User Account';
			break;
		case 'create_account':
			$controller = 'create_account';
			$h2 = 'Create a New Account';
			break;
		case 'view_account':
			$controller = 'view_account';
			$h2 = "View Account";
			break;
		case 'unit_circle':
			$controller = 'unit_circle';
			$h2 = 'The Unit Circle';
			break;
		case 'trig_functions':
			$controller = 'trig_functions';
			$h2 = 'Trigonometry Functions';
			break;
		default:
			$controller = 'about';
			$h2 = 'About Platonic Aether';
			break;
    }
}

$title .= ' - ' . $h2;

require_once("models/db.php");
require_once("models/session.php");

include_once("controllers/{$controller}.php");
// The controller will set the $content variable used by content_loader.
//	It will also provide functionality for the page, including loading appropriate models.

// header
include_once("views/templates/header.php");

// left nav bar
include_once("views/templates/nav_left.php");

// content
// This will use the $content variable which usually has the same value as $controller. It is set in the controller.
include_once("views/templates/content_well.php");

// footer
include_once("views/templates/footer.php");

?>
